# Scala Bioinformatics functions
These functions were written as I walked through the Bioinformatics I course on Coursera.
Scala is not one of the languages supported in the CS problems on their external site, but while going through the text I still enjoyed using Scala more than the typical options (e.g. Python, Java).

# TODOs
* I have to review how the Gibbs Sampler search works. My solution does *not* give a satisfactory result on the course site.

# Quick start
```scala
sbt console
import com.technallurgy.bio._
```
Then use any of the various functions within the package.

# Examples on using the "App" classes (and src/resources/)
* frequentWords: Finding all frequent patterns from freqwords_sample.in by using SBT's "run-main".
```bash
sbt "run-main com.technallurgy.bio.FrequentWordsApp" < src/main/resources/freqwords_sample.in
```
* patternMatch: Finding the pattern specified with `echo` within the Vibrio Cholerae genome.
```bash
echo 'CTTGATCAT' | cat - src/main/resources/Vibrio_cholerae.txt \
  | sbt "run-main com.technallurgy.bio.PatternMatchApp"
```
* clumpFinding: Using `echo` then `awk` to pass in some args (with a newline) to find clumps of 9-mers in the E-coli genome.
```bash
echo "9 500 3" | awk 'FNR==1{print ""}{print}' | cat src/main/resources/E-coli.txt - \
  | sbt "run-main com.technallurgy.bio.ClumpFindingApp"
```

# Sample of functions
For the below functions, `symbolMap` refers to a `Map[Char, Int]` which maps a set of characters to a numerical value.
The default used is `com.technallurgy.bio.nucleotides`, which maps each nucleotide (G,C,T,A) to an arbitrary value.

* `patternCount(text, pattern)`: Count how many times `pattern` occurs in `text`.
* `patternToNumber(pattern, symbolMap=com.technallurgy.bio.nucleotides)`: Convert a `pattern` into a numerical representation, where the base is equal to the number of symbols in the `symbolMap`.
* `numberToPattern(number, digits, symbolMap=com.technallurgy.bio.nucleotides)`: Convert a `number` back into a pattern with a specified number of `digits` and using the specified `symbolMap` to decode the number.
* `patternMatch(pattern, text)`: Return all indices in `text` where `pattern` exists, allowing overlaps.
* `computingFrequencies'(text, k, symbolMap=com.technallurgy.bio.nucleotides)`: Construct a frequency distribution of all patterns of length `k` in the `text`.
* `clumpFinding(text, k, minimumFrequency, windowSize, symbolMap=com.technallurgy.bio.nucleotides`: Find all patterns of length `k` which occur some `minimumFrequency` times within a sliding `windowSize` in a `text`.

