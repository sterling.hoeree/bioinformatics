package com.technallurgy.bio

/**
 * This app will figure out all of the most-frequently encountered kmers, given
 * two lines to stdin: the text, and the "k" in "kmer" (e.g. a 4-mer is 4 characters long).
 *
 * @example
 *   sbt "run-main com.technallurgy.bio.FrequentWordsApp" < src/main/resources/freqwords_sample.in
 *
 * @note Order of the output doesn't seem to match the sample... but that shouldn't matter, logically.
 *
 */
object FrequentWordsApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(text: String, k: String) = io.Source.stdin.getLines().toArray
    println( frequentWords(text, k.toInt) mkString " " )
  }

}
