package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * @todo Documentation
 * @example sbt "run-main com.technallurgy.bio.DeBrujin2App" < src/main/resources/dataset.txt
 */
object DeBrujinApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(k: String, text: String) = io.Source.stdin.getLines().toArray
    println( deBrujin(k.toInt, text) )
  }

}
