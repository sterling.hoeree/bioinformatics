package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * @todo Documentation
 * @example sbt "run-main com.technallurgy.bio.ApproxFrequentWordsApp" < src/main/resources/dataset_9_8.txt
 */
object ApproxFrequentWordsApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(text: String, kd: String) = io.Source.stdin.getLines().toArray
    val Array(k: String, d: String) = kd.split(" ")
    println( approxFrequentWords(text, k.toInt, d.toInt) mkString " " )
  }

}
