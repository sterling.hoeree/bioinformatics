package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * Prints all of the indeces in which a pattern appears in a genome.
 * The first line to stdin is the pattern, and the second is the entire genome to search.
 *
 * @example For the Vibrio Cholerae sample, do this:
 *          echo 'CTTGATCAT' | cat - src/main/resources/Vibrio_cholerae.txt \
 *            | sbt "run-main com.technallurgy.bio.PatternMatchApp"
 */
object PatternMatchApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(pattern: String, genome: String) = io.Source.stdin.getLines().toArray
    println( patternMatch(pattern, genome) mkString " " )
  }

}
