package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * @todo Write this documentation
 */
object FrequencyArrayApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(text: String, k: String) = io.Source.stdin.getLines().toArray
    println( computingFrequencies(text, k.toInt) mkString " " )
  }

}
