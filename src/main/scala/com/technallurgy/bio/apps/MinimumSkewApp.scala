package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * Given some input text, find all indices where there is the greatest difference of G-C in the text.
 * For example, in a genome CATGGGCATCGGCCATACGCC, the "skew" counts found is
 *  0 -1 -1 -1 0 1 2 1 1 1 0 1 2 1 0 0 0 0 -1 0 -1 -2
 * ... and the indices of the minimum values (troughs) of this array is at position 21.
 * @example echo CATGGGCATCGGCCATACGCC | sbt "run-main com.technallurgy.bio.MinimumSkewApp"
 */
object MinimumSkewApp extends App {

  override def main(args: Array[String]): Unit = {
    val text = io.Source.stdin.getLines().next()
    println( allMinIndices(skew(text)) mkString " " )
  }

}
