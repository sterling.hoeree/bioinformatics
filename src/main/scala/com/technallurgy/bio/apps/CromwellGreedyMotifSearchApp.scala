package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * This app will call the "greedyMotifSearch" method with the given arguments, but
 * also will set the default frequency for all nucleotides to 1:
 *   k, for the kmer size; t, for the number of rows; and then the actual DNA strings.
 */
object CromwellGreedyMotifSearchApp extends App {

  override def main(args: Array[String]): Unit = {
    val input = io.Source.stdin.getLines()
    val Array(k: String, t: String) = input.next().split(" ")
    val dna = input.toArray
    println( greedyMotifSearch(dna, k.toInt, smoothingFactor = 1.0)._2 mkString "\n" )
  }

}
