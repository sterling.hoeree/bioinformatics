package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * @todo Write this documentation
 */
object MostProbableByProfileApp extends App {

  override def main(args: Array[String]): Unit = {
    val input = io.Source.stdin.getLines()
    val text = input.next()
    val k = input.next().toInt
    val profile = Matrix.read[Double](input.mkString("\n"), sep=' ')
    println( mostProbableByProfile(text, k, profile) )
  }

}
