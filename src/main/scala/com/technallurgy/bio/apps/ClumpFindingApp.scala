package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * This app will find all "k-t clumps" in a window of size "L" from a given genome.
 * The first line of stdin is the genome to search.
 * The second line is "k t L" as integers, separated by spaces. "k" for "k-mer", "t" is the minimum
 * number of occurences for a clump to be detected, and "L" is the (sliding) window size.
 * @example For the E-Coli genome, do this:
 *          echo "9 500 3" | awk 'FNR==1{print ""}{print}' | cat src/main/resources/E-coli.txt - \
 *          | sbt "run-main com.technallurgy.bio.ClumpFindingApp"
 */
object ClumpFindingApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(text: String, klt: String) = io.Source.stdin.getLines().toArray
    val kltArgs = klt.split(" ")
    val k: Int = kltArgs(0).toInt
    val windowSize: Int = kltArgs(1).toInt
    val minFreq: Int = kltArgs(2).toInt

    println(s"Using args: k = $k, t = $minFreq, L = $windowSize")
    println( clumpFinding(text, k, minFreq, windowSize) mkString " " )
  }

}
