package com.technallurgy.bio.apps

import com.technallurgy.bio.{DNA, NoSuchNucleotide}

/**
 * Computes the complement DNA strand of the input given to this program (from stdin).
 * @note The complement DNA strand of a given strand of DNA is the complement of
 *       its nucleotides __in reverse order__ because DNA is always read in the 5' -> 3' direction.
 */
object ComplementApp extends App {

  override def main(args: Array[String]): Unit = {
    for (ln <- io.Source.stdin.getLines()) try {
      println(new DNA(ln).complement)
    } catch {
      case nsn: NoSuchNucleotide => println(nsn.getMessage)
    }
  }

}
