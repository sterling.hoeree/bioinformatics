package com.technallurgy.bio.apps

import com.technallurgy.bio.randomizedMotifSearch

/**
 * This app will call the "randomizedMotifSearch" method with the given arguments:
 *   k, for the kmer size; t, for the number of rows; and then the actual DNA strings.
 */
object RandomMotifSearchApp extends App {

  override def main(args: Array[String]): Unit = {
    val input = io.Source.stdin.getLines()
    val Array(k: String, t: String) = input.next().split(" ")
    val dna = input.toArray

    val init = (Integer.MAX_VALUE, List(""))
    val best = (0 to 3000).foldLeft(init)((bestResult, i) => {
      printf("\r%3d%% Complete", Math.round((i / 3000.0) * 100))
      val (score, motifs) = randomizedMotifSearch(dna, k.toInt)
      if (score < bestResult._1) (score, motifs) else bestResult
    })

    println("\nFinal best score found after 3000 iterations: " + best._1)
    println("Results:")
    println( best._2 mkString "\n" )

  }

}
