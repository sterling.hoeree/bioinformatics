package com.technallurgy.bio.apps

import com.technallurgy.bio.GibbsSampler

/**
 * This app will call the GibbsSampler object's apply() method with the given arguments:
 *   k, for the kmer size; t, for the number of rows; N for the number of iterations;
 *   and then the actual DNA strings.
 *
 *   XXX: TODO: Something is wrong with the GibbsSampler, or at least it doesn't seem to
 *   work for the Coursera course's grading system. It fails to return a sufficiently good result.
 *
 */
object GibbsSamplerSearchApp extends App {

  override def main(args: Array[String]): Unit = {
    val input = io.Source.stdin.getLines()
    val Array(k: String, t: String, n: String) = input.next().split(" ")
    val N = n.toInt
    val dna = input.toArray

    println("Iterations: " + 100 * N)

    val init = (Integer.MAX_VALUE, Array(""))
    val best = (0 to 100).foldLeft(init)((bestResult, i) => {
      printf("\r%3d%% Complete (%8d / %8d)", Math.round((i * N / (100.0 * N)) * 100), i * N, 100 * N)
      val (score, motifs) = GibbsSampler(dna, k.toInt, N)
      if (score < bestResult._1) (score, motifs) else bestResult
    })

    println(s"\nFinal best score found after ${100 * N} iterations: " + best._1)
    println("Results:")
    println( best._2 mkString "\n" )

//    println( "Result:" )
//    println( GibbsSampler(dna, k.toInt, n.toInt)._2 mkString "\n" )

  }

}
