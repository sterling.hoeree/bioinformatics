package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * Prints all of the indeces in which a pattern __approximately__ appears in a genome.
 * The first line to stdin is the pattern, the second is the entire genome to search, and
 * third is the maximum Hamming Distance (the maximum fuzziness).
 */
object ApproxPatternMatchApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(pattern: String, genome: String, d: String) = io.Source.stdin.getLines().toArray
    println( approxPatternMatch(pattern, genome, d.toInt) mkString " " )
  }

}
