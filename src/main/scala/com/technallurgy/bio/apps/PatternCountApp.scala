package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * This app takes two lines from stdin: the first is the text to search,
 * and the second is the pattern (a "kmer") to search for.
 *
 * @example
 *   sbt "run-main com.technallurgy.bio.PatternCountApp" < src/main/resources/dataset_2_6.txt
 *
 */
object PatternCountApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(text: String, pattern: String) = io.Source.stdin.getLines().toArray
    println( patternCount(text, pattern) )
  }

}
