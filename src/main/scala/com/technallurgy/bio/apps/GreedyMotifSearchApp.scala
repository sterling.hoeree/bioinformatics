package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * This app will call the "greedyMotifSearch" method with the given arguments:
 *   k, for the kmer size; t, for the number of rows; and then the actual DNA strings.
 */
object GreedyMotifSearchApp extends App {

  override def main(args: Array[String]): Unit = {
    val input = io.Source.stdin.getLines()
    val Array(k: String, t: String) = input.next().split(" ")
    val dna = input.toArray
    println( greedyMotifSearch(dna, k.toInt)._2 mkString "\n" )
  }

}
