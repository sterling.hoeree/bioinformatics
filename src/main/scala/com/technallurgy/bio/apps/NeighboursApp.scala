package com.technallurgy.bio.apps

import com.technallurgy.bio.neighbours

/**
 * @todo Documentation
 */
object NeighboursApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(pattern: String, d: String) = io.Source.stdin.getLines().toArray
    println( neighbours(pattern, d.toInt) mkString "\n" )
  }

}
