package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * @todo Documentation
 * @example sbt "run-main com.technallurgy.bio.CompositionApp" < src/main/resources/dataset.txt
 */
object CompositionApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(k: String, text: String) = io.Source.stdin.getLines().toArray
    println( composition(k.toInt, text) mkString "\n" )
  }

}
