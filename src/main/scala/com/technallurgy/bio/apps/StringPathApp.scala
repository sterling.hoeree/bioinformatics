package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * @todo Documentation
 * @example sbt "run-main com.technallurgy.bio.CompositionApp" < src/main/resources/dataset.txt
 */
object StringPathApp extends App {

  override def main(args: Array[String]): Unit = {
    val strings = io.Source.stdin.getLines().toArray
    println( stringPath(strings) )
  }

}
