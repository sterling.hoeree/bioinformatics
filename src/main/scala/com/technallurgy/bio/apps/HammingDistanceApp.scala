package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * @todo Write this documentation
 */
object HammingDistanceApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(text0: String, text1: String) = io.Source.stdin.getLines().toArray
    println( hammingDistance(text0, text1) )
  }

}
