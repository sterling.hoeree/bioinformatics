package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * @todo Documentation
 */
object BruteMedianStringApp extends App {

  override def main(args: Array[String]): Unit = {
    val input = io.Source.stdin.getLines().toList
    val k = input.head.toInt
    println( bruteMedianString(input.tail, k)._2 )
  }

}
