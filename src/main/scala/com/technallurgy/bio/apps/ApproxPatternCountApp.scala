package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * This app takes two lines from stdin: the first is the text to search,
 * the second is the pattern (a "kmer") to search for, and the third is the maximum
 * "distance" by which `pattern` can differ from a section of `text` to still count.
 *
 * @example printf "AAAAA\nAACAAGCTGATAAACATTTAAAGAG\n2" | sbt "run-main com.technallurgy.bio.ApproxPatternCountApp"
 */
object ApproxPatternCountApp extends App {

  override def main(args: Array[String]): Unit = {
    val Array(pattern: String, text: String, d: String) = io.Source.stdin.getLines().toArray
    println( approxPatternCount(text, pattern, d.toInt) )
  }

}
