package com.technallurgy.bio.apps

import com.technallurgy.bio._

/**
 * @todo Documentation
 */
object MotifEnumApp extends App {

  override def main(args: Array[String]): Unit = {
    val input = io.Source.stdin.getLines().toList
    val Array(k, d) = input.head.split(" ")
    println( motifEnum(input.tail, k.toInt, d.toInt) mkString " " )
  }

}
