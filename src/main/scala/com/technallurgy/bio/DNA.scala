package com.technallurgy.bio

import java.util.NoSuchElementException

object Nucleotide {
  val complementMap = Map('A' -> 'T', 'T' -> 'A', 'G' -> 'C', 'C' -> 'G')
  def complement(c: Char) = try {
    Nucleotide.complementMap(c)
  } catch { case _: NoSuchElementException => throw NoSuchNucleotide(c) }
}

class DNA(private val raw: String) {
  val strand: String = raw.toUpperCase
  lazy val complement: DNA =
    new DNA( strand.map(c => Nucleotide.complement(c)).reverse )

  override def toString = strand
  def dna_==(other: DNA): Boolean = strand.equals(other.strand) || complement.strand.equals(other.complement.strand)

  lazy private val h = new PatternHash(raw, nucleotides).hashCode
  override def hashCode: Int = h
  override def equals(o: Any) = hashCode == o.hashCode
}

class NoSuchNucleotide(message: String) extends Exception(message)
object NoSuchNucleotide {
  def apply(c: Char) = new NoSuchNucleotide(s"There is no such nucleotide '$c'. Valid nucleotides" +
    s" are 'G', 'C', 'T', or 'A'.")
}
