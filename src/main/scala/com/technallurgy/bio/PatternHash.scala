package com.technallurgy.bio

case class PatternHash(pattern: String, symMap: Map[Char, Int] = nucleotides) {
  lazy val h: Int = patternToNumber(pattern, symMap)
  override def hashCode: Int = h
}
