package com.technallurgy

import sun.security.provider.certpath.AdjacencyList

import scala.annotation.tailrec
import scala.collection.immutable.ListMap
import scala.collection.mutable
import scala.util.Random
import scala.util.control.Breaks._

package object bio {

  /**
   * Import data structures from subpackages.
   */
  import Matrix._

  /**
   * Default nucleotides available in a DNA strand, given arbitrary numerical values.
   * @note Whichever symbol set that is used should be in sorted order by the keys; greatest to least.
   */
  final val nucleotides = ListMap('T' -> 3, 'G' -> 2, 'C' -> 1, 'A' -> 0)

  type SymMap = Map[Char, Int]

  /**
   * Construct a new "Frequency Distribution" style map (String -> Int),
   * where by default every entry that doesn't exist when accessed will be initialized with a 0.
   * @return a new Map[String, Int]
   */
  def FreqDist[A] = mutable.LinkedHashMap[A, Int]().withDefaultValue(0)
  def ImmutableFreqDist[A] = ListMap[A, Int]().withDefaultValue(0)

  /**
   * Get the most frequent substrings of length k from the given text, with overlaps allowed.
   * @param text
   * @param k
   * @return
   */
  def frequentWords(text: String, k: Int): List[String] = {
    val freqDist =
      (0 to text.length - k).foldLeft(FreqDist[String])((fq, i) => {
        fq.updated(text.substring(i, i+k), 1 + fq(text.substring(i, i+k)))
      })

    val maxCount = freqDist.maxBy((x: (String, Int)) => x._2) ._2
    freqDist.toIterator.filter(_._2 == maxCount).map(_._1).toList
  }

  /**
   * Convert a String pattern into a number using a __sorted__ symbol map.
   * @param nuc
   * @param symMap
   * @return
   */
  def patternToNumber(nuc: String, symMap: SymMap = nucleotides): Int = {
    (nuc.length-1 to 0 by -1).zip(nuc).map { case (i: Int, n: Char) =>
      symMap(n) * math.pow(symMap.size, i).toInt
    } .sum
  }

  /**
   * Convert a number into a list of symbols of length `digits`, using a __sorted__ symbol map.
   * @param num
   * @param digits
   * @param symMap
   * @return
   */
  def numberToPattern(num: Int, digits: Int, symMap: ListMap[Char, Int] = nucleotides) = {
    val base = symMap.size
    val symbols = new StringBuilder()
    (digits-1 to 0 by -1).foldLeft(num)((n, i) => {
      n - symMap.foldLeft(0)((acc, t) =>
        if (acc == 0) {
          val sub = t._2 * math.pow(base, i).toInt
          if (n - sub >= 0) {
            symbols.append(t._1)
            sub
          } else acc
        } else acc)
    })
    symbols.toString()
  }

  /**
   * Alternative way of calculating the frequencies of each pattern from a text (allowing
   * overlaps in the sliding window for each pattern). It is probably less efficient than
   * `frequentWords` however.
   * @param text
   * @param k
   * @param symMap
   * @return
   */
  def computingFrequencies(text: String, k: Int, symMap: ListMap[Char, Int] = nucleotides) = {
    val arr = new Array[Int](math.pow(symMap.size, k).toInt)
    for (i <- 0 to (text.length - k)) {
      val pattern = text.substring(i, i+k)
      val code = patternToNumber(pattern)
      arr(code) += 1
    }
    arr
  }

  /**
   * An optimized version of `computingFrequencies` which uses a Scala HashMap instead
   * of the more memory-intensive Array of length `symMap.size^k`.
   * @param text
   * @param k
   * @param symMap
   * @return
   */
  def `computingFrequencies'`(text: String, k: Int, symMap: ListMap[Char, Int] = nucleotides) = {
    val freq = FreqDist[PatternHash]
    for (i <- 0 to (text.length - k)) {
      val pattern = text.substring(i, i+k)
      freq(new PatternHash(pattern)) += 1
    }
    freq
  }

  /**
   * Find the number of differences between `text0` and `text1`. The __length__ of each text __should match__.
   * @param text0 The first text.
   * @param text1 The second text.
   * @return The number of differences (Char mismatches) between each text,
   *         or -1 if the lengths of both texts __do not match__.
   */
  def hammingDistance(text0: String, text1: String): Int = {
    if (text0.length != text1.length) -1
    else {
      var d = 0
      text0.foldLeft(0)((i, next) => {
        d += (
          text0.charAt(i) match {
            case c if c != text1.charAt(i) => 1
            case _ => 0
          })
        i + 1
      })
      d
    }
  }

  /**
   * Return all of the indices for which (an approximate) `pattern` appears in `text`.
   * @param pattern
   * @param text
   * @param d The maximum difference in `pattern` vs. a section of `text` to still count as a "match".
   * @return All indices in `text` where (the approximate) `pattern` starts.
   */
  def approxPatternMatch(pattern: String, text: String, d: Int) =
    (0 to text.length - pattern.length).filter(i => hammingDistance(text.substring(i, i+pattern.length), pattern) <= d)

  /**
   * Return all of the indices for which `pattern` appears in `text`.
   * This is a special case of `approxPatternMatch` where `d=0` (that is, the `pattern` must appear __exactly__).
   * @param pattern
   * @param text
   * @return All indices in `text` where `pattern` starts.
   */
  def patternMatch(pattern: String, text: String) = approxPatternMatch(pattern, text, 0)

  /**
   * Count the number of times a pattern occurs in text, with overlaps allowed.
   * @param text
   * @param pattern
   * @param d The maximum difference in `pattern` vs. a section of `text` to still count as a "match".
   * @return
   */
  def approxPatternCount(text: String, pattern: String, d: Int): Int = approxPatternMatch(pattern, text, d).length

  /**
   * Count the number of times a pattern occurs in text, with overlaps allowed.
   * This is a special case of `approxPatternCount` where `d=0` (that is, the `pattern` must appear __exactly__).
   * @param text
   * @param pattern
   * @return
   */
  def patternCount(text: String, pattern: String): Int = approxPatternCount(text, pattern, 0)

  /**
   * Find all "clumps" of `k`-mers in the `genome` which occur at least `minFreq` times for each
   * sliding `windowSize`.
   * @param genome
   * @param k
   * @param minFreq
   * @param windowSize
   */
  def clumpFinding(genome: String, k: Int, minFreq: Int, windowSize: Int,
                   symMap: ListMap[Char, Int] = nucleotides): Set[String] = {
    // Set up the first window of the genome to search in.
    val freqPatterns = `computingFrequencies'`(genome.substring(0, windowSize-1), k, symMap)

    // Clumps will be stored in a frequency distribution as well.
    val clumps = FreqDist[PatternHash]

    // Find all clumps from the first pattern frequency calculation.
    freqPatterns.filter(t => t._2 >= minFreq).foreach(t => clumps(t._1) += 1)
    //bugaliboo_is_beautiful//

    // Slide the window forward, 1 symbol at a time.
    // This means that the first k-mer from the previous frequency distribution will be decreased by 1, and the
    // newest symbol encountered will form a new k-mer:
    //               0 1 2 3 4 5 6 7  8
    //   Pass #0:   |A C C G T A C A| G
    //   Pass #1: A |C C G T A C A G| T <-- So "G" is added and "A" is lost.
    //               1 2 3 4 5 6 7 8  9
    //              |<---Window---->| k = 2, windowSize = 8
    //
    for (i <- 1 to genome.length - windowSize) {
      val lostPattern = new PatternHash( genome.substring(i-1, i-1+k) )
      freqPatterns(lostPattern) -= 1 // Decrease the frequency of the lost pattern.
      val newPattern = new PatternHash( genome.substring(i+windowSize-k, i+windowSize) )
      freqPatterns(newPattern) += 1 // Increase the frequency of the new pattern.
      if (freqPatterns(newPattern) >= minFreq) clumps(newPattern) += 1
    }

    // Now get out just those (minFreq,windowSize) clumps that we found (clump >= 1).
    clumps.filter(t => t._2 > 0).keys.map(h => h.pattern).toSet
  }

  /**
   * Create a line-graph (represented by an `Array` of integers) showing the difference
   * between two symbols (`sym0 - sym1`) starting from a difference of 0 and moving along
   * through the `text` (genome) until the end is reached.
   * @param text The text (genome) to iterate through.
   * @param sym0 Default: 'G'. The first symbol, which will be compared against the second symbol, `sym1`.
   * @param sym1 Default: 'C'. The second symbol.
   * @return An `Array[Int]` showing the differences `sym0 - sym1` along `text`.
   */
  def skew(text: String, sym0: Char = 'G', sym1: Char = 'C'): Array[Int] = {
    val arr = new Array[Int](text.length + 1)

    text.foldLeft(0)((i, next) => {
      arr(i+1) = arr(i) + (
        next match {
          case sym if sym == sym0 => 1
          case sym if sym == sym1 => -1
          case _ => 0
        })
      i + 1
    })

    arr
  }

  /**
   * Search an Array of integers, returning the indices of all of the minimum elements.
   * For example, given [1, 1, 2, 1, 4], this would return [0, 1, 3].
   * @param arr The Array[Int] to search.
   * @return A List[Int] of the indices for all occurrences of the minimum value in `arr`.
   * @example `allMinIndices(skew(text)) .mkString(" ")`
   */
  def allMinIndices(arr: Array[Int]): List[Int] = {
    @tailrec def find(i: Int, x: (Int, List[Int])): (Int, List[Int]) = {
      if (i < arr.length) arr(i) match {
        case d if d < x._1 => find(i+1, (d, List(i)))
        case d if d == x._1 => find(i+1, (d, x._2 ++ List(i)))
        case _ => find(i+1, x)
      } else x
    }
    find(1, (arr(0), List(0))) ._2 // Return just the list of indices
  }

  /**
   * @todo Documentation
   * @param k
   * @param symMap
   * @return
   */
  def generatePatterns(k: Int, symMap: SymMap = nucleotides): List[String] = {
    def generate(size: Int): Iterable[List[Char]] = {
      if (size == 0) List(List())
      else for {
        x <- symMap.keys
        xs <- generate(size - 1)
      } yield x :: xs
    }
    generate(k).map(_.mkString).toList
  }

  /**
   * Object which calculates neighbours of some pattern up to "d", and caches
   * the result for (pattern, d) since it is a very expensive calculation.
   */
  object neighbours {
    val cache = mutable.Map[String, List[String]]()

    var cacheCall = 0
    var cacheMiss = 0

    def apply(pattern: String, d: Int): List[String] = {
      cacheCall += 1
      cache.getOrElseUpdate(s"$pattern-$d", {
          cacheMiss += 1
          generatePatterns(pattern.length).filter(p => hammingDistance(p, pattern) <= d)
        }
      )
    }
  }

  def mkApproxFreqDist(text: String, k: Int, d: Int, allowReverse: Boolean = true) =
    (0 to text.length - k).foldLeft(ImmutableFreqDist[DNA])((fq, i) => {
      val ns = ( neighbours(text.substring(i, i+k), d) ++
        (if (allowReverse) neighbours(new DNA(text.substring(i, i+k)).complement.strand, d) else List()))
        .map(x => new DNA(x))

      ns.foldLeft(fq)((fqAcc, kmer) => fqAcc.updated(kmer, fqAcc(kmer) + 1))
    })

  /**
   * @todo Documentation
   * @param text
   * @param k
   * @param d
   * @return
   */
  def approxFrequentWords(text: String, k: Int, d: Int, allowReverse: Boolean = true) = {
    val frequencies: Map[DNA, Int] = mkApproxFreqDist(text, k, d, allowReverse)
    val maxVal = frequencies.maxBy(_._2)
    frequencies.filter(_._2 == maxVal._2).map(_._1).toList
  }

  /**
   * @todo Documentation
   * @param dna
   * @param k
   * @param d
   */
  def motifEnum(dna: Iterable[String], k: Int, d: Int) = {
    val freqDists = dna.map(text => mkApproxFreqDist(text, k, d, allowReverse = false))
    freqDists.flatten.groupBy(_._1)  // Flatten all entries into (DNA, occurrences)
      .filter(_._2.size == dna.size) // Only return those entries that occurred at least once in all "dna" strings
      .map(_._1) // Project just the patterns
  }

  /**
   * Take the mathematical log of an arbitrary base.
   * @param x
   * @param base
   * @return
   */
  def log(x: Double, base: Double) = math.log(x) / math.log(base)

  /**
   * Calculate the entropy of a collection of doubles.
   * Entropy = H(x) = - Sum from (i <- 0 to N) of ( probability(x_i) * log_2(x_i) )
   * @param ns
   */
  def entropy(ns: Iterable[Double]): Double = -1 * ns.filter(_ > 0).foldLeft(0.0d)((H, x) => H + log(x, 2))

  /**
   * Return the sum of all Hamming Distances between pattern and dna, with overlaps.
   * @param pattern
   * @param texts
   * @return
   */
  def minDist(pattern: String, texts: Iterable[String]) = {
    def `minDist'`(dna: String) = (0 to dna.length - pattern.length)
      .foldLeft(Integer.MAX_VALUE)((dist, i) => {
        val check = hammingDistance(pattern, dna.substring(i, i + pattern.length))
        if (dist > check) check else dist
      })

    texts.foldLeft(0)((acc, dna) => acc + `minDist'`(dna))
  }

  /**
   * Get out the pattern with the minimum distance across all "dna" strings.
   * @param dna
   * @param k
   * @return
   */
  def bruteMedianString(dna: Iterable[String], k: Int) = {
    val init = (Integer.MAX_VALUE, "")
    generatePatterns(k).foldLeft(init)((pair, pattern) => {
      val (oldMin, _) = pair
      val dist = minDist(pattern, dna)
      if (dist < oldMin) (dist, pattern) else pair
    })
  }

  /**
   * Find probability of `pattern` occurring given some `profile`.
   * @param pattern
   * @param profile
   * @param rowLabels
   * @return
   */
  def probability(pattern: String, profile: Matrix[Double],
                  rowLabels: SymMap = Map('A'->0,'C'->1,'G'->2,'T'->3)): Double = {
    if (pattern.length != profile.head.length) 0.0 else {
      (0 to pattern.length-1).foldLeft(1.0)((p, i) => {
        val lookup = pattern.charAt(i)
        profile(rowLabels(lookup), i) * p
      })
    }
  }

  /**
   * Get the most probable k-mer from a 4 x k profile matrix (which contains probabilities).
   * We know that the 4 rows will correspond to the nucleotides {A, C, G, T} in that order.
   * @param text
   * @param k
   * @param profile
   */
  def mostProbableByProfile(text: String, k: Int, profile: Matrix[Double],
                            rowLabels: SymMap = Map('A'->0,'C'->1,'G'->2,'T'->3)) = {
    val init: (Double, Option[String]) = (0.0, None) // probability, pattern
    (0 to text.length - k).foldLeft(init)((pair, i) => {
      val (oldMax, oldPattern) = pair
      val pattern = text.substring(i, i+k)
      val p = probability(pattern, profile, rowLabels = rowLabels)
      if (oldPattern.isEmpty || p > oldMax) (p, Some(pattern)) else pair
    }) ._2.get // Get out the k-mer with the maximum probability in the text.
  }

  def scoreString(s: String) = {
    val freqDist = s.foldLeft(FreqDist[Char])((fq, a) => fq.updated(a, 1 + fq(a)))
    (freqDist, (freqDist - freqDist.maxBy(_._2)._1).foldLeft(0)((sum, t) => sum + t._2))
  }

  def scoreMotifs(motifs: Iterable[String]): Int = {
    val m = Matrix.explode[String](motifs)
    m.head.indices.foldLeft(0)( (score, colN) => score + scoreString(m.col(colN) mkString "")._2 )
  }

  object Profile {
    def fromFreqs(frequencies: Iterable[Map[Char, Int]], smoothingFactor: Double = 0.0): Matrix[Double] = {
      val probabilities = frequencies.foldLeft(mutable.ArrayBuffer[Array[Double]]())((pr, next) => {

        val total = next.foldLeft(0)((sum, x) => sum + x._2) + (if (smoothingFactor > 0) 4 else 0)
        val freqA = (next('A').toDouble + smoothingFactor) / total /* Note: the order here is important. */
        val freqC = (next('C').toDouble + smoothingFactor) / total /*       It corresponds to the order  */
        val freqG = (next('G').toDouble + smoothingFactor) / total /*       given in the Coursera text.  */
        val freqT = (next('T').toDouble + smoothingFactor) / total
        pr :+ Array(freqA, freqC, freqG, freqT)
      })

      Matrix.colwise[Double](probabilities.toIndexedSeq)
    }

    def apply(motifs: Iterable[String], smoothingFactor: Double = 0.0): Matrix[Double] = {
      val m = Matrix.explode[String](motifs)
      val frequencies = m.head.indices.foldLeft(mutable.ListBuffer[Map[Char, Int]]())((fqs, colN) => {
        val fq = scoreString(m.col(colN) mkString "")._1
        fqs :+ fq.toMap.withDefaultValue(0)
      })
      fromFreqs(frequencies, smoothingFactor)
    }
  }

  /**
   * @todo Documentation
   * @param dna
   * @param k
   * @return The best (score, motifs list) found
   */
  def greedyMotifSearch(dna: Iterable[String], k: Int, smoothingFactor: Double = 0.0) = {
    val init = (Integer.MAX_VALUE, List[String]())

    (0 to dna.head.length - k).foldLeft(init)((oldBest, i) => {
      val pattern = dna.head.substring(i, i+k)
      val motifs = dna.tail.foldLeft(mutable.ListBuffer(pattern))((motifs, text) => {
        val profile = Profile(motifs, smoothingFactor)
        val motif = mostProbableByProfile(text, k, profile)
        motifs :+ motif
      })
      val newScore = scoreMotifs(motifs)
      if (oldBest._1 > newScore) (newScore, motifs.toList) else oldBest
    })
  }

  object randomizedMotifSearch {
    lazy val rand = new Random(System.currentTimeMillis())

    /**
     * Search for approximately optimal kmers from a set of DNA strings.
     * @param dna
     * @param k
     * @return The best motifs list found
     */
    def apply(dna: Iterable[String], k: Int) = {
      val initKmers = dna.foldLeft(mutable.ListBuffer[String]())((kmers, next) => {
        val startIdx = rand.nextInt(next.length - k)
        kmers :+ next.substring(startIdx, startIdx+k)
      }).toList

      var motifs = initKmers
      var bestMotifs = initKmers
      var bestScore = scoreMotifs(motifs)

      // TODO: It'd be great to figure out how to do this without an infinite loop and yucky breaking.
      breakable {
        do {
          val profile = Profile(motifs, smoothingFactor = 1.0)
          motifs = dna.map(text => mostProbableByProfile(text, k, profile)).toList

          val score = scoreMotifs(motifs)
          if (score < bestScore) {
            bestMotifs = motifs
            bestScore = score
          } else break()
        } while (true)
      }

      (bestScore, bestMotifs)
    }
  }

  object MotifGenerator {
    lazy val rand = new Random(System.currentTimeMillis())

    def weightedChoice(weights: Seq[Double]): Int = {
      val total = weights.sum
      val r = rand.nextDouble()

      var percent = 0.0
      var i = 0
      breakable {
        for (w <- weights.toIterator.map(_ / total)) {
          percent += w
          if (r < percent) break()
          i += 1
        }
      }
      i // Return the index
    }

    def apply(profile: Matrix[Double], rowLabels: List[Char] = List('A','C','G','T')) = {
      require(profile.length == rowLabels.length)
      profile.head.indices.map(n => {
        val index = weightedChoice(profile.col(n))
        rowLabels(index)
      }) mkString ""
    }
  }

  object GibbsSampler {
    lazy val rand = new Random(System.currentTimeMillis())

    def apply(dna: Iterable[String], k: Int, N: Int) = {
      val initKmers = dna.foldLeft(mutable.ArrayBuffer[String]())((kmers, next) => {
        val startIdx = rand.nextInt(next.length - k)
        kmers :+ next.substring(startIdx, startIdx+k)
      }).toArray

      val t = initKmers.length
      val motifs = initKmers
      var bestMotifs = motifs
      var bestScore = scoreMotifs(motifs)

      (1 to N).foreach(i => {
        val r = rand.nextInt(t)
        val indices = (0 to r-1) ++ (r+1 to t-1)
        val profile = Profile(indices.map(i => motifs(i)), smoothingFactor = 1.0)
//        val profile = Profile(indices.map(i => motifs(i)))
        motifs(r) = MotifGenerator(profile)

        val score = scoreMotifs(motifs)
        if (score < bestScore) {
          bestMotifs = motifs
          bestScore = score
        }

//        printf("\r%3d%% Complete", Math.round((i / N.toDouble) * 100))
      })
//      println()

      (bestScore, bestMotifs)

    }

  }

  def composition(k: Int, text: String) =
    (0 to text.length-k).map(i => text.substring(i, i+k))

  def stringPath(strings: Array[String]): String =
    ((0 to strings.length - 2).map(i => strings(i).head) ++ strings.last)
      .mkString("")

  class AdjacencyList(adjacency: Map[String, List[String]], size: Int) {
    override def toString: String = {
      adjacency.map(e => e._2.mkString(","))
        .zip(adjacency.keys)
        .map(listAndKey => listAndKey._2 + " -> " + listAndKey._1)
        .mkString("\n")
    }
    def pop(k: String): (AdjacencyList, Option[String]) = {
      adjacency.get(k) match {
        case None => (this, None)
        case Some(ls) =>
          if (ls.size > 1)
            (AdjacencyList(adjacency.updated(k, ls.tail), size), Some(ls.head))
          else
            (AdjacencyList(adjacency - k, size-1), Some(ls.head))
      }
    }
  }

  object AdjacencyList {
    def apply(adjacency: Map[String, List[String]], size: Int) = new AdjacencyList(adjacency, size)
    def apply(adjacency: Map[String, List[String]]) = new AdjacencyList(adjacency, adjacency.size)
  }

  def eulerianCycle(adj: AdjacencyList) = {
    val cycle = mutable.LinkedHashMap[String, (String, String)]()
    // TODO
  }

  def overlap(strings: IndexedSeq[String]) = {
    val prefixes = mutable.Map[String, List[String]]()
    val suffixes = mutable.Map[String, List[String]]()
    val adjacency = mutable.Map[String, List[String]]()
    strings.foreach(s => {
      val prefix = s.init
      val suffix = s.tail

      prefixes.get(suffix) match {
        case None => ()
        case Some(prefixMatch: List[String]) => prefixMatch.foreach(connectingString =>
          adjacency.update(s, adjacency.getOrElse(s, List()) ++ List(connectingString))
        )
      }

      prefixes.update(prefix, prefixes.getOrElse(prefix, List()) ++ List(s))

      suffixes.get(prefix) match {
        case None => ()
        case Some(suffixMatch: List[String]) => suffixMatch.foreach(connectorString =>
          adjacency.update(connectorString, adjacency.getOrElse(connectorString, List()) ++ List(s))
        )
      }

      suffixes.update(suffix, suffixes.getOrElse(suffix, List()) ++ List(s))
    })

    AdjacencyList(adjacency.toMap)
  }

  def deBrujin(k: Int, text: String): AdjacencyList = {
    require(k > 1, "k must be > 1")
    val reads = composition(k, text)
    deBrujin(reads)
  }

  def deBrujin(reads: IndexedSeq[String]): AdjacencyList = {
    val adjacency = mutable.Map[String, List[String]](reads.head.init -> List(reads.head.tail))
    reads.tail.foreach(s => {
      adjacency.update(s.init, adjacency.getOrElse(s.init, List()) ++ List(s.tail))
    })

    AdjacencyList(adjacency.toMap)
  }

}
