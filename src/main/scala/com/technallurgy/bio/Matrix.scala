package com.technallurgy.bio

import scala.collection.mutable
import scala.reflect.ClassTag

/**
 * @author Created by sterling on 25/02/16.
 */

class Matrix[A](m: Array[Matrix.Row[A]]) extends Iterable[Matrix.Row[A]] {
  require(m.find(r => r.size == 0).isEmpty)

  def iterator = m.iterator

  val length = m.length // Number of rows

  def apply(row: Int, column: Int): A = m(row)(column)

  def col(n: Int) = {
//    println(s"Matrix.col($n), where m = ${toString()}")
    m.indices.map(i => {
//      println(s"Matrix.col($n).m($i): ${m(i)}")
      m(i)(n)
    })
  }

  override def toString(): String = "[ " + ( m.map(r => r mkString " ") mkString "\n  " ) + " ]"
}

object Matrix {
//  type Row[A] = mutable.ArraySeq[A]
  type Row[A] = IndexedSeq[A]

  trait Parsable[A] {
    def parse(str: String): A
  }

  implicit val intParser = new Parsable[Int] {
    override def parse(str: String): Int = str.toInt
  }

  implicit val doubleParser = new Parsable[Double] {
    override def parse(str: String): Double = str.toDouble
  }

  implicit val identityParser = new Parsable[String] {
    override def parse(str: String) = str
  }

  // TODO: add more Parsables if needed.

  /**
   * Read a csv string and convert it into a Matrix of type `A`.
   * @param csv String to read.
   * @param sep Separator character to use (default is comma).
   * @param parser Implicit parser which converts String -> A.
   * @tparam A The type of the Matrix.
   * @return
   */
  def read[A](csv: String, sep: Char = ',')(implicit parser: Parsable[A]): Matrix[A] = {
    new Matrix[A](
        csv.split('\n').map(row =>
          row.split(sep).map(x => parser.parse(x)).asInstanceOf[Row[A]]
        )
    )
  }

  /**
   * Explode a series of Strings into a Matrix of type A.
   * @example explode( ["AAG", "GAT"] ) into Matrix[String]: will become [ [A, A, G]; [G, A, T] ]
   * @param it
   * @param parser
   * @tparam A
   * @return
   */
  def explode[A](it: Iterable[String])(implicit tag: ClassTag[A], parser: Parsable[A]): Matrix[A] = {
    new Matrix[A](
      it.filterNot(_.isEmpty).foldLeft(mutable.ArrayBuffer[Row[A]]())((arrs, str) =>
        arrs :+ str.iterator.foldLeft(mutable.ArrayBuffer[A]())((arr, a) => {
          arr += parser.parse(a.toString)
        }).toIndexedSeq
      ).toArray[Row[A]]
    )
  }

  /**
   * Create a Matrix from a list of columns instead of rows.
   * @param cols
   * @tparam A
   * @return
   */
  def colwise[A](cols: Seq[Array[A]])(implicit tag: ClassTag[A]): Matrix[A] = {
    val rows = new Array[Array[A]](cols.head.length)
    cols.indices.foreach(colN => {
      cols(colN).indices.foreach(rowN => {
        if (colN == 0) rows(rowN) = new Array[A](cols.length)
        val column: Array[A] = cols(colN)
        val elem: A = column(rowN)
        val rowToUse: Array[A] = rows(rowN)
        rowToUse(colN) = elem
      })
    })

    val rows2 = rows.map(_.toIndexedSeq.asInstanceOf[Row[A]])
    new Matrix[A](rows2)
  }

}
